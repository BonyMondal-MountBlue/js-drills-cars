// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
//const myModule= require('../Inventory/inventory');
//const mymodule = require('./Inventory');
//let val=myModule.inventory
module.exports={
problem1: function (val){
    if(val.length===0 || val===null || val===undefined){
        return [];
    }
    for(var j=0;j<val.length;j++){
        if (val[j].id == 33){
            //return "Car " + val[j].id + " is a " + val[j].car_year + " " + val[j].car_make +" "+ val[j].car_model;
            //var result=`Car ${val[j].id} is a ${val[j].car_year} ${val[j].car_make} ${val[j].car_model}`;
            return val[j]
            }
        }
    }
}
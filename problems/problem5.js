// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
let c=0
module.exports={
    problem5: function(val){
        if(val.length===0){
            return [];
        }
        for(let i=0;i<val.length;i++){
            if (val[i]<2000){
                c=c+1;
            }
        }
        return c;
    }
}